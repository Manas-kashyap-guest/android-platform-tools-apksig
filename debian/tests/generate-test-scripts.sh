#!/bin/sh -e
#

set -x
test -d ../../debian/tests || (echo "run in debian/tests/ subdir of package"; exit 1)
basedir=$(cd $(dirname $0)/../..; pwd)

cat <<EOF > $basedir/debian/tests/ApkVerifierTest.sh
#!/bin/sh -e
set -x

# test -J args from apksigner shell script
apksigner -JXmx256M verify src/test/resources/com/android/apksig/v1-only-with-rsa-pkcs1-sha384-1.2.840.113549.1.1.1-1024.apk

EOF

cd $basedir

for f in $(ls -1 `grep -A1 'assertVerifiedForEach(' src/test/java/com/android/apksig/ApkVerifierTest.java \
  | grep -Eo '"v[12]-.*\.apk"' \
  | sed -e 's,%s,[0-9p][0-9]*,' -e 's,^",src/test/resources/com/android/apksig/,' -e 's,",,g'`); do
    echo apksigner verify $f  >> $basedir/debian/tests/ApkVerifierTest.sh
done

for f in `grep -A1 'assertVerificationFailure(' src/test/java/com/android/apksig/ApkVerifierTest.java \
  | grep -Eo ' "v[12]-.*\.apk"' \
  | sed -e 's,^ ",src/test/resources/com/android/apksig/,' -e 's,",,g'`; do
    echo '! apksigner verify' $f  >> $basedir/debian/tests/ApkVerifierTest.sh
done

echo "echo SUCCESS" >> $basedir/debian/tests/ApkVerifierTest.sh
